import { Injectable } from '@angular/core';
import {DialogBoxComponent} from '../dialog-box-component/dialog-box.component';
import {MatDialog} from '@angular/material';

@Injectable({
  providedIn: 'root'
})
export class DialogService {

  constructor(public dialog: MatDialog) {
  }

  public goToDialogBox(callback): void {
    const dialogRef = this.dialog.open(DialogBoxComponent,
        {
          width: '300px',
        });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        callback();
      }
    });
  }
}
