import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {TasksListingService} from '../service/tasks-listing.service';
import {FormControl, FormGroup, Validators} from '@angular/forms';

@Component({
    selector: 'app-comment',
    templateUrl: './comment.component.html',
    styleUrls: ['./comment.component.scss']
})
export class CommentComponent implements OnInit {

    public formGroup: FormGroup;
    @Input() selectedId;
    @Output() commentShows: EventEmitter<any> = new EventEmitter();

    constructor(
        private route: ActivatedRoute,
        private tasksListing: TasksListingService
    ) {
    }

    ngOnInit() {
        this.initForm();
    }

    private initForm() {
        this.formGroup = new FormGroup({
            comment: new FormControl('', Validators.required)
        });
    }

    public addComment() {
        this.tasksListing.addComment(this.selectedId, this.formGroup.get('comment').value);
        this.commentShows.emit(Event);
    }

    public cancelComment(item) {
        this.commentShows.emit(item);
    }
}
