import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateOrEditTaskComponent } from './create-or-edit-task.component';

describe('CreateOrEditTaskComponent', () => {
  let component: CreateOrEditTaskComponent;
  let fixture: ComponentFixture<CreateOrEditTaskComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateOrEditTaskComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateOrEditTaskComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
