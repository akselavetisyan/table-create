import {Component, OnInit} from '@angular/core';
import {Location} from '@angular/common';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {TasksListingService} from '../service/tasks-listing.service';
import {ActivatedRoute} from '@angular/router';
import {tasks} from '../models/tasks';

@Component({
    selector: 'app-create-or-edit-task',
    templateUrl: './create-or-edit-task.component.html',
    styleUrls: ['./create-or-edit-task.component.scss']
})
export class CreateOrEditTaskComponent implements OnInit {

    public formGroup: FormGroup;
    public idExists;

    constructor(
        private route: ActivatedRoute,
        private location: Location,
        private tasksListing: TasksListingService
    ) {
    }

    ngOnInit() {
        this.initForm();
        this.pageDecision();
    }

    public back() {
        this.location.back();
    }

    private initForm() {
        this.formGroup = new FormGroup({
            name: new FormControl('', Validators.required)
        });
    }

    private pageDecision() {
        this.idExists = !this.route.snapshot.params.id;
    }

    public CreateOrEditTask() {
        const name = this.formGroup.get('name').value;
        if (this.route.snapshot.params.id) {
            this.tasksListing.editTask(this.route.snapshot.params.id, name);
        } else {
            this.tasksListing.addTask(name);
            window.localStorage.setItem('tasks', JSON.stringify(this.tasksListing.tasks));
        }
        this.back();
    }
}
