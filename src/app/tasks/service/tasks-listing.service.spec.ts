import { TestBed } from '@angular/core/testing';

import { TasksListingService } from './tasks-listing.service';

describe('TasksListingService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TasksListingService = TestBed.get(TasksListingService);
    expect(service).toBeTruthy();
  });
});
