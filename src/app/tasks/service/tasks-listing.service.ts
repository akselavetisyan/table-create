import {Injectable} from '@angular/core';
import {tasks} from '../models/tasks';
import {DialogService} from '../../main/dialog-box/service/dialog.service';

@Injectable({
    providedIn: 'root'
})
export class TasksListingService {

    public tasks = JSON.parse(window.localStorage.getItem('tasks')) ? JSON.parse(window.localStorage.getItem('tasks')) : tasks;

    constructor(
        private dialogService: DialogService
    ) {
    }

    public addTask(name) {
        this.tasks.push({name});
        return tasks;
    }

    public editTask(id, name) {
        this.tasks[id].name = name;
        window.localStorage.setItem('tasks', JSON.stringify(this.tasks));
    }

    public deleteItem(id) {
        this.dialogService.goToDialogBox(() => {
            this.tasks.splice(id, 1);
            window.localStorage.setItem('tasks', JSON.stringify(this.tasks));
        });
    }

    public addComment(id, value) {
        if (!this.tasks[id].comments) {
            this.tasks[id].comments = [];
        }
        this.tasks[id].comments.push(value);
        window.localStorage.setItem('tasks', JSON.stringify(this.tasks));
    }
}
