import {Component, OnInit} from '@angular/core';
import {environment} from '../../../environments/environment.prod';
import {Router} from '@angular/router';
import {TasksListingService} from '../service/tasks-listing.service';

@Component({
    selector: 'app-task-listing',
    templateUrl: './task-listing.component.html',
    styleUrls: ['./task-listing.component.scss']
})
export class TaskListingComponent implements OnInit {

    public env = environment;
    public selectedId: number;
    public selectedComment: number;
    public commentShows = false;
    public commentShowsCondition = false;
    public tasks = this.tasksListing.tasks;

    constructor(
        private router: Router,
        private tasksListing: TasksListingService
    ) {
    }

    ngOnInit() {
    }

    public navigate(url, id) {
        this.router.navigate([url + id]);
    }

    public delete(id) {
        this.tasksListing.deleteItem(id);
    }

    public openComment(id) {
        this.selectedId = id;
        this.commentShows = true;
    }

    public hideComment(item) {
        this.commentShows = item;
    }

    public showOrHideComment(id) {
        this.commentShowsCondition = !this.commentShowsCondition;
        const comment = this.selectedComment;
        this.selectedComment = id;
        if (comment !== this.selectedComment && comment !== undefined) {
            this.commentShowsCondition = !this.commentShowsCondition;
        }
    }
}
