import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {TaskListingComponent} from './task-listing/task-listing.component';
import {CreateOrEditTaskComponent} from './create-or-edit-task/create-or-edit-task.component';
import {CommentComponent} from './comment/comment.component';


const routes: Routes = [
  {
    path: '',
    component: TaskListingComponent
  },
  {
    path: 'create-task',
    component: CreateOrEditTaskComponent
  },
  {
    path: 'edit-task/:id',
    component: CreateOrEditTaskComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TasksRoutingModule { }
