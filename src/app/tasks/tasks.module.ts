import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {TasksRoutingModule} from './tasks-routing.module';
import {TaskListingComponent} from './task-listing/task-listing.component';
import {CreateOrEditTaskComponent} from './create-or-edit-task/create-or-edit-task.component';
import {ReactiveFormsModule} from '@angular/forms';
import {CommentComponent} from './comment/comment.component';


@NgModule({
    declarations: [
        TaskListingComponent,
        CreateOrEditTaskComponent,
        CommentComponent
    ],
    imports: [
        CommonModule,
        TasksRoutingModule,
        ReactiveFormsModule
    ]
})
export class TasksModule {
}
